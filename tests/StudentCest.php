<?php


namespace Tests\Api;

use Tests\Support\ApiTester;
use \Codeception\Util\HttpCode;

class StudentCest
{
    const DEFAULT_URL = 'store/order/';
    const ID = 1;
    const INVALID_ID = 'invaliddata';

    const REQUEST_BODY = [
        'id' => '1',
        'petId' => '0',
        'quantity' => '0',
        'shipDate' => '2023-05-21T06:24:20.035Z',
        'status' => 'placed',
        'complete' => true,
    ];


    // tests
    public function testPostOrderUnsupportedMediaType(ApiTester $I)
    {
        //Создание новой записи с неподдерживаемым типом данных
        $I->haveHttpHeader('accept', '*/*');
        $I->haveHttpHeader('content-type', '*/*');
        $I->sendPost(self::DEFAULT_URL, self::REQUEST_BODY);
        $I->seeResponseCodeIs(HttpCode::UNSUPPORTED_MEDIA_TYPE);
    }

    public function testPostOrderSuccess(ApiTester $I)
    {
        //Создание новой записи
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost(self::DEFAULT_URL, self::REQUEST_BODY);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'petId' => 'integer',
            'quantity' => 'integer',
            'shipDate' => 'string:date',
            'status' => 'string',
            'complete' => 'boolean'
        ]);
    }

    public function testPostOrderInvalid(ApiTester $I)
    {
        //Создание новой записи с неправильным типом данных
        $I->haveHttpHeader('accept', 'application/xml');
        $I->haveHttpHeader('content-type', 'application/xml');
        $I->sendPost(self::DEFAULT_URL, self::REQUEST_BODY);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }

    public function testPostOrderNotAcceptable(ApiTester $I)
    {
        //Создание новой записи с недопустимым типом данных
        $I->haveHttpHeader('accept', 'text/*');
        $I->haveHttpHeader('content-type', 'text/*');
        $I->sendPost(self::DEFAULT_URL, self::REQUEST_BODY);
        $I->seeResponseCodeIs(HttpCode::NOT_ACCEPTABLE);
    }

    public function testGetOrderSuccess(ApiTester $I)
    {
        //Корректное получение созданой записи
        $I->sendGet(self::DEFAULT_URL.self::ID);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'petId' => 'integer',
            'quantity' => 'integer',
            'shipDate' => 'string:date',
            'status' => 'string',
            'complete' => 'boolean'
        ]);
    }

    public function testGetOrderNotFound(ApiTester $I)
    {
        //Получение несуществующей записи
        $I->sendGet(self::DEFAULT_URL.self::INVALID_ID);
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function testGetOrderNotAllowed(ApiTester $I)
    {
        //Получение записи без указания id
        $I->sendGet(self::DEFAULT_URL);
        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED);
    }

    public function testGetOrderInvalid(ApiTester $I)
    {
        //Получение записи с неверным указанием id
        $I->sendGet(self::DEFAULT_URL.'>%00'.self::ID);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }
}
